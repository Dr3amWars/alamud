# -*- coding: utf-8 -*-
# Copyright (C) 2020 Mathieu Hallez, IUT d'Orléans
#==============================================================================

from .action import Action1
from mud.events import HelpEvent

class HelpAction(Action1):
    EVENT = HelpEvent
    ACTION = "help"
