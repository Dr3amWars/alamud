# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class ChopWithEvent(Event3):
    NAME = "chop-with"
    
    def perform(self):
        if not self.object.has_prop("chopable-with"):
            self.fail()
            return self.inform("chop-with.failed")
        if not self.object2.has_prop("choper"):
            self.fail()
            return self.inform("chop-with.failed")
        self.inform("chop-with")