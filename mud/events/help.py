# -*- coding: utf-8 -*-
# Copyright (C) 2020 Mathieu Hallez, IUT d'Orléans
#==============================================================================

from .event import Event1

class HelpEvent(Event1):
    NAME = "help"

    def perform(self):
        self.buffer_clear()
        self.buffer_append("test.ok")